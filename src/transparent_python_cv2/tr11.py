def image_segmentation(ip_convert):
    img = cv2.imdecode(np.squeeze(np.asarray(ip_convert[1])), 1)


    # cv2.imwrite("Skin_removed.jpg",img_skin)

    height, width, channels = img.shape
#     blurred = cv2.GaussianBlur(img, (5, 5), 0)
#
    mask = np.zeros(img.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    rect = (5, 5, width - 5, height - 5)
    cv2.grabCut(img, mask, rect, bgdModel, fgdModel, 2, cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
    img_mask = img* mask2[:, :, np.newaxis]
    cv2.imwrite("download(8)_grab.jpg",img_mask)
    # cv2.waitKey(0)
    # blurred = cv2.GaussianBlur(img_mask,(3,3),0)
    # img_skin = skin_detector.process(img_mask)
    # cv2.imwrite("download(10)_skin.jpg",img_skin)
    # cv2.waitKey(0)
    blurred = cv2.GaussianBlur(img_mask,(5,5),0)
    edgeImg = np.max( np.array([ edgedetect(blurred[:,:, 0]), edgedetect(blurred[:,:, 1]), edgedetect(blurred[:,:, 2]) ]), axis=0 )
    mean = np.mean(edgeImg);
# # Zero any value that is less than mean. This reduces a lot of noise.
    edgeImg[edgeImg < mean] = 0;
    edgeImg_8u = np.asarray(edgeImg, np.uint8)

# # Find contours
    significant = findSignificantContours(img_mask, edgeImg_8u, edgeImg)
    cv2.imwrite("download(8)_contour.jpg",significant)
    significant = cv2.GaussianBlur(significant,(3,3),0)
    tmp = cv2.cvtColor(significant, cv2.COLOR_BGR2GRAY)
    _, alpha = cv2.threshold(tmp, 0, 1, cv2.THRESH_BINARY)
    b, g, r = cv2.split(significant)
    rgba = [b, g, r, alpha]
    dst = cv2.merge(rgba, 4)
    img_out = cv2.imencode('.png', dst)
    # cv2.imshow("Masking_Done.jpg",dst)
    # cv2.waitKey(0)
    return img_out
