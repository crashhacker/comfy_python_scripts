def run(self, ips, snap, img, para = None):
        msk = ips.mark.buildmsk(img.shape)
        bgdModel = np.zeros((1,65),np.float64)
        fgdModel = np.zeros((1,65),np.float64)
        msk, bgdModel, fgdModel = cv2.grabCut(snap, msk,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
        img[msk%2 == 0] //= 3
